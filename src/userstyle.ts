import { UserInfo } from "./services/users";

export function computeMessageStyles(info: UserInfo) {
  let bgcol = "";
  const bg = info.background;
  if (bg.enabled) {
    bgcol = ` background-color: ${bg.color}`;
  }
  return `color: ${info.textColor};${bgcol}`;
}

export function computeMessageBackgroundStyles(backgroundUrl: string, info: UserInfo) {
  const bg = info.background;
  if (bg.enabled) {
    let bgAlign;
    if (bg.align === "top-left") {
      bgAlign = "top left";
    }
    else if (bg.align === "top-right") {
      bgAlign = "top right";
    }
    else if (bg.align === "bottom-left") {
      bgAlign = "bottom left";
    }
    else if (bg.align === "bottom-right") {
      bgAlign = "bottom right";
    }
    const bgUrl = bg.image ? `url(${backgroundUrl})` : "none";
    let bgRepeat = "";
    if (bg.repeat.x) {
      if (bg.repeat.y) {
        bgRepeat = "repeat";
      }
      else {
        bgRepeat = "repeat-x";
      }
    }
    else if (bg.repeat.y) {
      bgRepeat = " repeat-y";
    }
    else {
      bgRepeat = "no-repeat";
    }
    const bgSize = bg.size.x + " " + bg.size.y;
    return `opacity: ${bg.alpha / 100}; background-image: ${bgUrl}; background-position: ${bgAlign}; background-repeat: ${bgRepeat}; background-size: ${bgSize}`;
  }
  else {
    return "";
  }
}

export function computeHeaderStyles(info: UserInfo) {
  return `color: ${info.nameColor}`;
}

export function computeRosterItemStyles(info: UserInfo) {
  if (info.background.enabled) {
    return `color: ${info.nameColor}; background-color: ${info.background.color}`;
  }
  else {
    return "";
  }
}
