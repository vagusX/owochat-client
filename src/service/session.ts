import { Endpoint } from "../util/http";

export default class Session {
  public readonly authorization: string;
  public readonly myUsername: string;
  public readonly deviceToken: string;

  public constructor(authorization: string, myUsername: string, deviceToken: string) {
    this.authorization = authorization;
    this.myUsername = myUsername;
    this.deviceToken = deviceToken;
  }

  public call<Req, Res>(endpoint: Endpoint<Req, Res>, req: Req): Promise<Res> {
    return endpoint.call({ authorization: this.authorization }, req);
  }
}
