export class Timeout {
  private _timeout: number | null;
  private _callback: () => void;

  public constructor(callback: () => void) {
    this._timeout = null;
    this._callback = callback;
  }

  public schedule(millis: number) {
    if (this._timeout != null) {
      clearTimeout(this._timeout);
    }
    this._timeout = setTimeout(() => {
      this._timeout = null;
      this._callback();
    }, millis);
  }

  public isScheduled() {
    return this._timeout !== null;
  }

  public clear() {
    if (this._timeout !== null) {
      clearTimeout(this._timeout);
      this._timeout = null;
    }
  }
}

export default Timeout;
