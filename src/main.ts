import App from './components/App.svelte';

// TODO: typing state notifs
// TODO: better color picker

const app = new App({
	target: document.body,
	props: {}
});

import service from "./service";
import { endpoints } from "./service/endpoints";

(window as any).owo = {
  service,
  endpoints,
  createInvite: async (autoAdd: boolean) => {
    const { inviteToken } = await service.createInvite({ autoAdd });
    return window.location.origin + "/invite/" + inviteToken;
  },
};

async function installSw() {
  const reg = await navigator.serviceWorker.register("/sw.js");
  navigator.serviceWorker.addEventListener("message", (evt) => {
    const data = evt.data;
    switch (data.type) {
      case "reload":
        window.location.reload();
        break;
    }
  });
  navigator.serviceWorker.startMessages();
}

if (localStorage.getItem("install-sw")) {
  installSw();
}

addEventListener("beforeinstallprompt", (event) => {
  service.onBeforeInstallPrompt(event);
  event.preventDefault();
});

export default app;
